package com.example.app.dao;

import com.example.app.model.Courses;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class CoursesDaoImpl implements CoursesDAO{

  private JdbcTemplate jdbcTemplate;
  private StudentDAO studentDAO;

  public CoursesDaoImpl(DataSource dataSource){
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  @Override
  public List<Courses> coursesList(int id){
    String sql = "select \n" +
            "\tcs.namecs,\n" +
            "\tcs.durationcs, \n" +
            "\tcs.freeplacescs \n" +
            "from \n" +
            "\tstudents st, courses cs, studentscourses sc\n" +
            "where \n" +
            "\tst.idst=?\n" +
            "and\n" +
            "\tst.idst=sc.idst\n" +
            "and \n" +
            "\tcs.idcs=sc.idcs";
    List<Courses> coursesList = jdbcTemplate.query(sql, new Object[]{id}, (rs,  rowNum) -> {

      Courses courses = new Courses();
      courses.setNamecs(rs.getString("namecs"));
      courses.setDuration(rs.getString("durationcs"));
      courses.setFreePlaces(rs.getInt("freeplacescs"));
      courses.setFreePlaces(id);

      return courses;
    });
    return coursesList;
  }

  @Override
  public void addCoursesToStudent(int id) {

  }

  public void addCoursesToStudent(int studentId, int coursesId){
    /*Courses course = getCourse(coursesId);
    if (course.getFreePlaces() > 0) {
      jdbcTemplate.query(sql*/
    }


  public void removeCoursesFromStudent(int id){

  }

  @Override
  public Courses getCourse(int id) {
    return null;
  }


}
