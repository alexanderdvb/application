package com.example.app.model;

public class Courses {

  private int idcs;
  private String namecs;
  private String duration;
  private int freePlaces;


  public Courses(){
  }

  public Courses(int idcs, String namecs, String duration, int freePlaces) {
    this.idcs = idcs;
    this.namecs = namecs;
    this.duration = duration;
    this.freePlaces = freePlaces;
  }

  public int getIdcs() {
    return idcs;
  }

  public void setIdcs(int idcs) {
    this.idcs = idcs;
  }

  public String getNamecs(String namecs) {
    return this.namecs;
  }

  public void setNamecs(String namecs) {
    this.namecs = namecs;
  }

  public String getDuration(String durationcs) {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public int getFreePlaces() {
    return freePlaces;
  }

  public void setFreePlaces(int freePlaces) {
    this.freePlaces = freePlaces;
  }
}
